import pandas as pd
import requests
import xml.etree.ElementTree as ET
import dimension
import os
from os import listdir
from os.path import isfile
import time

import json
import glob
from pandas import DataFrame, read_csv, read_sql_query, to_datetime, Timestamp, Timedelta

dim = dimension.Connect('/home/jovyan/projects/config_txt.m')
#dim.create('Bike_forTraining')
#dim.create('BikeStation_forTraining')

def populate_stations(filename):
    '''Load bike statation data from json file and write to table in database '''

#     engine = create_engine('mysql+mysqldb://root:s2dspassword1@104.196.168.234/BikeData')

    stations = json.load(open(filename, 'r'))
    stationsDF = DataFrame(stations['stations']['station'])
    
    #Drop columns which are transient properties or aren't actually recorded for most stations
    stationsDF = stationsDF[['id', 'installDate', 'lat', 'long', 'name', 'nbDocks', 'removalDate']] 

    # Remove stations that do no exist in the time period we are looking at
    stationsDF['id'] = stationsDF['id'].astype('int')
    stationsDF = stationsDF[stationsDF.id<790]

    stationsDF['lat'] = stationsDF['lat'].astype('float')
    stationsDF['long'] = stationsDF['long'].astype('float')
    stationsDF['nbDocks'] = stationsDF['nbDocks'].astype('int')

#     stationsDF.to_sql('Stations', engine, if_exists='append')
    records = json.loads(stationsDF.T.to_json()).values()
    dim.db()['BikeStation_forTraining'].insert_many(records, ordered = True)
    
def populate_journeys(file, collection):
    '''Load historical data into SQL database and clean data '''
    
#     stations = read_sql_query("SELECT id FROM Stations", engine)['id'].tolist()
    stations = dim.db()['BikeStation_forTraining'].find({},{'_id':0})
    stations = list(stations)
    stations = pd.DataFrame(stations)['id'].tolist()
    stations.append(0)

    cols=['Rental Id', 'Duration','Bike Id', 'End Date', 'EndStation Id', 'Start Date' , 'StartStation Id']
    
    HistData= read_csv(file, usecols=cols,na_values=['Tabletop1'])
    HistData = HistData.rename(columns={c: c.replace(' ', '_') for c in HistData.columns})
    
    try:
        HistData['End_Date'] = to_datetime(HistData['End_Date'], format='%d/%m/%Y %H:%M')
    except:
        HistData['End_Date'] = to_datetime(HistData['End_Date'], format='%d/%m/%Y %H:%M:%S') #Some of the Data has seconds in the date column
    
    try:
        HistData['Start_Date'] = to_datetime(HistData['Start_Date'], format='%d/%m/%Y %H:%M')
    except:
        HistData['Start_Date'] = to_datetime(HistData['Start_Date'], format='%d/%m/%Y %H:%M:%S')
    
    
    for i, row in HistData[HistData.Bike_Id != HistData.Bike_Id].iterrows():
        HistData.loc[i,'Bike_Id']=0
    
    
    for i, row in HistData[HistData.EndStation_Id != HistData.EndStation_Id].iterrows(): #Newer data marks unkown stations as NaN
        HistData.loc[i,'EndStation_Id']=0
   
    for i, row in HistData[HistData.StartStation_Id != HistData.StartStation_Id].iterrows(): #Newer data marks unkown stations as NaN 
        HistData.loc[i,'StartStation_Id']=0
        
    #Fix End Times for journeys that only have and start time and a duration
    for i, row in HistData[(HistData.End_Date < Timestamp('2012-01-01 00:00:00'))&(HistData.Start_Date > Timestamp('2012-01-01 00:00:00'))&(HistData.Duration>0)].iterrows():
        HistData.loc[i,'End_Date']= row.Start_Date + Timedelta(seconds=row.Duration)
        
    for i, row in HistData[(HistData.End_Date > Timestamp('2012-01-01 00:00:00'))&(HistData.Start_Date < Timestamp('2012-01-01 00:00:00'))&(HistData.Duration>0)].iterrows():
        HistData.loc[i,'Start_Date']= row.End_Date - Timedelta(seconds=row.Duration)
    
    for i, row in HistData[HistData.Duration<0].iterrows():
        HistData.loc[i,'Duration']=0
    

    HistData['EndStation_Id'] = HistData['EndStation_Id'].astype('int')
    HistData['StartStation_Id'] = HistData['StartStation_Id'].astype('int')
    HistData['Bike_Id'] = HistData['Bike_Id'].astype('int')
    HistData = HistData[(HistData.StartStation_Id != 0)&(HistData.Bike_Id != 0)&(HistData.EndStation_Id.isin(stations))&(HistData.StartStation_Id.isin(stations))]

#     HistData.to_sql('Journeys', engine, if_exists='append', chunksize=10000)
    records = HistData.to_dict(orient = 'records')
    dim.db()[collection].insert_many(records, ordered = True)   
    
    
def load_all_data(collection):
    FileNames = glob.glob('data/historical_csv/bike_data/*.csv')
    for file in FileNames:
        populate_journeys(file, collection)
        print(file)
        
        
if __name__ == "__main__":
    collection = 'Bike_forTraining'
    populate_stations('data/bikeStation.json')
    print('stations populated')
    load_all_data(collection)
    print('journeys populated')