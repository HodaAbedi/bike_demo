# get_ipython().magic('matplotlib inline')
import matplotlib
matplotlib.use('Agg')

import pandas as pd
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt 

#the input file contains the flow in and our for 4 previous time step for a week ago, 3 days ago,
#2 days ago, yesterday, today. The order is: flow-out4, flow-in4, out3,, in3, out2, in2, out1, in1
# this data set is already suffled.

InputData=pd.read_csv("DeletedStationsRushHour15minsInput.csv", header=None)
OutputData =pd.read_csv("DeletedStationsRushHour15minsOutput.csv", header=None)

training_size=InputData.shape[0]


#Parameters
learning_rate = 0.01

# adaptive learning rate (haven't tested it yet)
#learning_rate = tf.placeholder(tf.float32, shape=[])

VALIDATION_SIZE = int(training_size*0.2)
training_epochs = 50
batch_size = 100
total_batch= int(training_size / batch_size)
DROPOUT = 1.0
# The random seed that defines initialization.
SEED = 73

# Network Parameters
station_number=543

input_dimension = (station_number)*16+3
output_dimension = (station_number)*2

# number of units at each hidden layer:
n_hidden_1 = 2048 
n_hidden_2 = 2048
n_hidden_3 = 2048
n_hidden_4 = 2048

# Batching the data:

InputData['Index'] = InputData.index
OutputData['Index'] = OutputData.index

InputData['Batch']=InputData['Index'].apply(lambda x : np.mod(x,total_batch)) 
OutputData['Batch']=OutputData['Index'].apply(lambda x : np.mod(x,total_batch)) 

# Dividing the data set into a training set and validation set:

InputData_validation= InputData[:VALIDATION_SIZE]
OutputData_validation= OutputData[:VALIDATION_SIZE]
InputData= InputData[VALIDATION_SIZE:]
OutputData= OutputData[VALIDATION_SIZE:]



# Create a placeholder for our input and output variables as well as the dropouts
X = tf.placeholder(tf.float32, [None, input_dimension])
Y = tf.placeholder(tf.float32, [None, output_dimension])
keep_prob = tf.placeholder("float")


# Create variables for our neural net weights and bias
#for four-layer neural network:

weights= {
    'w_1' : tf.Variable(tf.random_normal([input_dimension, n_hidden_1],stddev=0.1,seed=SEED)),
    'w_2' : tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2],stddev=0.1,seed=SEED+1)),
    'w_3' : tf.Variable(tf.random_normal([n_hidden_2, n_hidden_3],stddev=0.1,seed=SEED+2)),
    'w_4' : tf.Variable(tf.random_normal([n_hidden_3, n_hidden_4],stddev=0.1,seed=SEED+4)),
    'w_out' : tf.Variable(tf.random_normal([n_hidden_4, output_dimension],stddev=0.1,seed=SEED+3))
}

biases = {
    'b_input': tf.Variable(tf.random_normal([n_hidden_1], 0, 0.1)),
    'b_hidden1': tf.Variable(tf.random_normal([n_hidden_2], 0, 0.1)),
    'b_hidden2': tf.Variable(tf.random_normal([n_hidden_3], 0, 0.1)),
    'b_hidden3': tf.Variable(tf.random_normal([n_hidden_4], 0, 0.1)),
    'b_hidden4': tf.Variable(tf.random_normal([output_dimension], 0, 0.1))
}
#-------------------------------------------------------------------------------
# Define the  model

def multilayer_perceptron(x, weights, biases):
# Hidden layer with RELU activations

    layer_1 = tf.add(tf.matmul(x, weights['w_1']), biases['b_input'])
    layer_1_drop = tf.nn.dropout(layer_1, keep_prob)
    layer_1_drop = tf.nn.relu(layer_1_drop)
    
    layer_2 = tf.add(tf.matmul(layer_1_drop, weights['w_2']), biases['b_hidden1'])
    layer_2_drop = tf.nn.dropout(layer_2, keep_prob)
    layer_2_drop = tf.nn.relu(layer_2_drop)
    
    
    layer_3 = tf.add(tf.matmul(layer_2_drop, weights['w_3']), biases['b_hidden2'])
    layer_3_drop = tf.nn.dropout(layer_3, keep_prob)
    layer_3_drop = tf.nn.relu(layer_3_drop)

    layer_4 = tf.add(tf.matmul(layer_3_drop, weights['w_4']), biases['b_hidden3'])
    layer_4_drop = tf.nn.dropout(layer_4, keep_prob)
    layer_4_drop = tf.nn.relu(layer_4_drop)

#    Since this is a regression problem the activation function
# for the output layer is equal to identity   
    out_layer = tf.add(tf.matmul(layer_4_drop, weights['w_out']), biases['b_hidden4'])


    return out_layer
#------------------------------------------------------------------------------------------


pred = multilayer_perceptron(X, weights, biases)


# Define cost function and optimizer:

# cost function is penalised for zero values:
cost = tf.reduce_mean(tf.mul(tf.square(pred-Y),tf.square(tf.add(Y, tf.ones_like(Y)))))

# RMSE cost function:
#cost = tf.sqrt(tf.reduce_mean(tf.square(tf.sub(Y,pred))))  

optimizer = tf.train.AdagradOptimizer(learning_rate=learning_rate).minimize(cost)





# Initializing the variables

init = tf.initialize_all_variables()
sess = tf.Session()
sess.run(init)


# 
f1 = open("costs",'w')
f2 = open("av_costs",'w')
f3 = open("av_accu", 'w')
f4 = open("accu",'w')

f2.write("#LR:%.4f, H layers:%d %d %d %d, dropout:%.1f, input units:%d \n" %(learning_rate, n_hidden_1, n_hidden_2, n_hidden_3, n_hidden_4, DROPOUT, input_dimension))
#
f1.write("training_cost validation_cost iteration \n")
f2.write("training_cost validation_cost iteration \n")
f3.write("accuracy_training accuracy_validation iteration \n")
f4.write("accuracy_training accuracy_validation iteration \n")


f1.close()
f2.close()
f3.close()
f4.close()


# Perform our training runs

for epoch in range(training_epochs):
    avg_train=0.
    avg_val=0.
    avg_acc_val=0.
    avg_acc_train= 0.

    for i in range(total_batch):
        InputData2 = InputData[InputData.Batch == i]
        OutputData2 = OutputData[OutputData.Batch == i]
        InputData_validation2 = InputData_validation[InputData_validation.Batch == i]
        OutputData_validation2 = OutputData_validation[OutputData_validation.Batch == i]
  

        batch_inputs = np.asarray(InputData2.drop(['Index','Batch'], axis=1))        
        batch_outputs = np.asarray(OutputData2.drop(['Index','Batch'], axis=1))

# perform back propagation using the batched training set:

        sess.run(optimizer, feed_dict={X: batch_inputs, Y: batch_outputs, keep_prob: DROPOUT})


        val_inputs = np.asarray(InputData_validation2.drop(['Index','Batch'], axis=1))        
        val_outputs = np.asarray(OutputData_validation2.drop(['Index','Batch'], axis=1)) 

        print ("batch=", i)

# Calculate the cost for the training and validation sets:

        cost_train=sess.run(cost,feed_dict={X: batch_inputs, Y: batch_outputs, keep_prob: 1.0})
        cost_val=sess.run(cost,feed_dict={X: val_inputs, Y: val_outputs, keep_prob: 1.0})


# get the predictions for the validation set:

        prediction=sess.run(pred, feed_dict={X: val_inputs, Y: val_outputs, keep_prob: 1.0})
        print (prediction, val_outputs)


# Evaluate the accuracy for both training and validation sets:

        accu = tf.reduce_mean(tf.abs(tf.sub(Y, pred)))
        accuracy_train = sess.run(accu, feed_dict={X: batch_inputs, Y: batch_outputs, keep_prob: 1.0})
        accuracy_val = sess.run(accu, feed_dict={X: val_inputs, Y: val_outputs, keep_prob: 1.0})


# calculate average cost and accuracy for the whole trainig set and validation set per iteration:
        avg_train += cost_train / total_batch
        avg_val += cost_val / total_batch

        avg_acc_train += accuracy_train / total_batch
        avg_acc_val += accuracy_val / total_batch


        f1 = open("costs2",'a')
        f1.write("%.3f %.3f %d \n"% (cost_train, cost_val, (i+1)+(epoch*total_batch)))
        f1.close()

        f4 = open("accu2",'a')
        f4.write("%.3f %.3f %d \n"% (accuracy_train, accuracy_val, (i+1)+(epoch*total_batch)))
        f4.close()



    print ("------------------------------------------------")    
    print ("epoch=", epoch)
    print ("mean training cost=", avg_train)
    print ("mean validation cost=", avg_val)
    print ("------------------------------------------------")    

    f2 = open("av_costs",'a')
    f2.write("%.6f %.6f %d \n"% (avg_train, avg_val, epoch))
    f2.close()

    f3 = open("av_accu",'a')
    f3.write("%.3f %.3f %d \n"% (avg_acc_train, avg_acc_val, epoch))
    f3.close()

# save a checkpoint file per epoch  with properties of the neural network: weights, biases, etc. 
    saver = tf.train.Saver()
    save_path = saver.save(sess, "AllStations.ckpt",global_step=epoch)
    print("Model saved in file: %s" % save_path)
