# Restoring the properties of the neural netwrok from the saved checkpoint file.
# test our NN model 

import matplotlib
matplotlib.use('Agg')

import pandas as pd
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt 

#the input file contains the flow in and our for 4 previous time step for a week ago, 3 days ago,
#2 days ago, yesterday, today. The order is: flow-out4, flow-in4, out3,, in3, out2, in2, out1, in1
# for only one day (29/02/2016).

#InputData=pd.read_csv("TestStationsRemoved290216Input.csv", header=None)
#OutputData =pd.read_csv("TestStationsRemoved290216Output.csv", header=None)


InputData=pd.read_csv("testing_Aug1_7_15minInput.csv", header=None)
OutputData =pd.read_csv("testing_Aug1_7_15minOutput.csv", header=None)

training_size=InputData.shape[0]


#Parameters
learning_rate = 0.01

#if you want to have adaptive learning rate (note sure if works)
#learning_rate = tf.placeholder(tf.float32, shape=[])
DROPOUT = 1.0
# The random seed that defines initialization.
SEED = 73



# Network Parameters
station_number=543

input_dimension = (station_number )*16+3
output_dimension = (station_number)*2

n_hidden_1 = 2048 
n_hidden_2 = 2048 
n_hidden_3 = 2048
n_hidden_4 = 2048
n_hidden_5 = 2048




# Create a placeholder for our input and expected variables
X = tf.placeholder(tf.float32, [None, input_dimension])
Y = tf.placeholder(tf.float32, [None, output_dimension])
keep_prob = tf.placeholder("float")


weights= {
    'w_1' : tf.Variable(tf.random_normal([input_dimension, n_hidden_1],stddev=0.1,seed=SEED)),
    'w_2' : tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2],stddev=0.1,seed=SEED+1)),
    'w_3' : tf.Variable(tf.random_normal([n_hidden_2, n_hidden_3],stddev=0.1,seed=SEED+2)),
    'w_4' : tf.Variable(tf.random_normal([n_hidden_3, n_hidden_4],stddev=0.1,seed=SEED+4)),
    'w_out' : tf.Variable(tf.random_normal([n_hidden_4, output_dimension],stddev=0.1,seed=SEED+3))
}

biases = {
    'b_input': tf.Variable(tf.random_normal([n_hidden_1], 0, 0.1)),
    'b_hidden1': tf.Variable(tf.random_normal([n_hidden_2], 0, 0.1)),
    'b_hidden2': tf.Variable(tf.random_normal([n_hidden_3], 0, 0.1)),
    'b_hidden3': tf.Variable(tf.random_normal([n_hidden_4], 0, 0.1)),
    'b_hidden4': tf.Variable(tf.random_normal([output_dimension], 0, 0.1))
}


def multilayer_perceptron(x, weights, biases):
    # Hidden layer with RELU activation
    layer_1 = tf.add(tf.matmul(x, weights['w_1']), biases['b_input'])
    layer_1_drop = tf.nn.dropout(layer_1, keep_prob)
    layer_1_drop = tf.nn.relu(layer_1_drop)

    layer_2 = tf.add(tf.matmul(layer_1_drop, weights['w_2']), biases['b_hidden1'])
    layer_2_drop = tf.nn.dropout(layer_2, keep_prob)
    layer_2_drop = tf.nn.relu(layer_2_drop)
    
    
    layer_3 = tf.add(tf.matmul(layer_2_drop, weights['w_3']), biases['b_hidden2'])
    layer_3_drop = tf.nn.dropout(layer_3, keep_prob)
    layer_3_drop = tf.nn.relu(layer_3_drop)

    layer_4 = tf.add(tf.matmul(layer_3_drop, weights['w_4']), biases['b_hidden3'])
    layer_4_drop = tf.nn.dropout(layer_4, keep_prob)
    layer_4_drop = tf.nn.relu(layer_4_drop)

    out_layer = tf.add(tf.matmul(layer_4_drop, weights['w_out']), biases['b_hidden4'])


    return out_layer




pred = multilayer_perceptron(X, weights, biases)
# Define loss and optimizer
  
cost = tf.reduce_mean(tf.multiply(tf.square(pred-Y),tf.square(tf.add(Y, tf.ones_like(Y)))))

optimizer = tf.train.AdagradOptimizer(learning_rate=learning_rate).minimize(cost)


# restore the variables:

saver = tf.train.Saver()

with tf.Session() as sess:
        saver.restore(sess, "model/precious.ckpt-24")
        print("Model restored.")

# introduce the validation set and calculate the cost function for them.

        val_inputs = np.asarray(InputData)        
        val_outputs = np.asarray(OutputData) 

        cost_val=sess.run(cost,feed_dict={X: val_inputs, Y: val_outputs, keep_prob: 1.0})

# get the predictions for the validation set
        prediction=sess.run(pred, feed_dict={X: val_inputs, Y: val_outputs, keep_prob: 1.0})

# define accuracy as MSE or abs(Y-Pred) / Y
        accu = tf.sqrt(tf.reduce_mean(tf.square(tf.subtract(Y,pred))))
#        accu = tf.reduce_mean(tf.abs(tf.sub(Y, pred)))
        accuracy_val = sess.run(accu, feed_dict={X: val_inputs, Y: val_outputs, keep_prob: 1.0})

        print ("validation cost, accuracy=", cost_val, accuracy_val)

# save the Y  values and predictions in separate files.
# the columns include: flow_out station#1, flow_in station #1, ..., flow_out station#543, flow_in station#543 
# rows include: flow data for 15min increments.

        np.savetxt("prediction",prediction, fmt= '%0.4f', delimiter=",")
        np.savetxt("actual",val_outputs, fmt= '%d', delimiter=",")
