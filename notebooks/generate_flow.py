import pandas as pd
from sqlalchemy import create_engine # database connection
import datetime as dt
import glob
import numpy as np
import calendar
import json
import dimension
from datetime import datetime
    

def flow(stationlist, engine, interval, output):
    '''Take list of stations (or lists of stations) and produce flow in and out data in time intervals specified'''

    intervalSeconds=interval*60

    TimeStep= pd.date_range("2012/1/4", "2016/2/21", freq = str(interval)+"min")

    TimeFrame = pd.DataFrame(index = range(len(TimeStep)), columns = ['Date', 'DoW', 'Month'])
    TimeFrame['Date'] = TimeStep

    TimeFrame['DoW'] = TimeFrame['Date'].apply(lambda x: x.to_datetime().date().weekday())
    TimeFrame['Month'] = TimeFrame['Date'].apply(lambda x: x.to_datetime().date().month)
    Flows = TimeFrame
    
    
    for i in stationlist:
        
        if isinstance(i, int):
            if i % 50==0:
                print(str(i))
            queryIn = "SELECT UNIX_TIMESTAMP(Start_Date) DIV %d AS Date, count(*) as Flow_In_%d FROM Journeys WHERE EndStation_Id=%d GROUP BY UNIX_TIMESTAMP(Start_Date) DIV %d" % (intervalSeconds, i, i, intervalSeconds)
            queryOut = "SELECT UNIX_TIMESTAMP(Start_Date) DIV %d AS Date, count(*) as Flow_Out_%d FROM Journeys WHERE StartStation_Id=%d AND EndStation_Id<>0 GROUP BY UNIX_TIMESTAMP(Start_Date) DIV %d" % (intervalSeconds, i, i, intervalSeconds)
        else:
            queryIn = "SELECT UNIX_TIMESTAMP(Start_Date) DIV %d AS Date, count(*) as Flow_In_%d FROM Journeys WHERE EndStation_Id IN (" % (intervalSeconds, i[0])
            queryOut = "SELECT UNIX_TIMESTAMP(Start_Date) DIV %d AS Date, count(*) as Flow_Out_%d FROM Journeys WHERE StartStation_Id IN(" % (intervalSeconds, i[0])      
            for j, item in enumerate(i):
                if j==len(i)-1:
                    queryIn += str(item)
                    queryOut += str(item)
                else:
                    queryIn += str(item) + ","
                    queryOut += str(item) + ","
            
            queryIn += ") GROUP BY UNIX_TIMESTAMP(Start_Date) DIV %d" % intervalSeconds
            queryOut += ") AND EndStation_Id<>0 GROUP BY UNIX_TIMESTAMP(Start_Date) DIV %d" % intervalSeconds
        FlowIn = pd.read_sql_query(queryIn, engine)
        FlowOut = pd.read_sql_query(queryOut, engine)


        FlowIn['Date'] = pd.to_datetime(FlowIn['Date'] * intervalSeconds, unit='s')
        FlowOut['Date'] = pd.to_datetime(FlowOut['Date'] * intervalSeconds, unit='s')  

        Flows = pd.merge(Flows, FlowOut, on = 'Date', how = 'outer')
        Flows = pd.merge(Flows, FlowIn, on = 'Date', how = 'outer')

    Flows.fillna(0, inplace = True)
    
    Flows.to_csv(output)

    
def flow_nosql(stationlist, start_date, end_date, interval, output, collection):
    '''Take list of stations (or lists of stations) and produce flow in and out data in time intervals specified'''

    intervalSeconds=interval*60*1000

    TimeStep= pd.date_range(start_date, end_date, freq = str(interval)+"min")

    TimeFrame = pd.DataFrame(index = range(len(TimeStep)), columns = ['Date', 'DoW', 'Month'])
    TimeFrame['Date'] = TimeStep

    TimeFrame['DoW'] = TimeFrame['Date'].apply(lambda x: x.to_datetime().date().weekday())
    TimeFrame['Month'] = TimeFrame['Date'].apply(lambda x: x.to_datetime().date().month)
    Flows = TimeFrame
    
    dim = dimension.Connect('/home/jovyan/projects/config_txt.m')

    for i in stationlist:
        print(i)
        
        if isinstance(i, int):
            '''when a date object is subtracted (or other direct math operation) from another date object, then the result is a numeric value representing the epoch timestamp milliseconds between the two objects. So just using the epoch date you get the epoch milliseconds representation.'''

            result_in = dim.db()[collection].aggregate([
                { "$group": {
                    "_id": { "$subtract": [{ "$subtract": [ "$End_Date",datetime(1970,1,1) ] }, { "$mod": [{ "$subtract": [ "$End_Date",datetime(1970,1,1) ] },interval]} ] },
                    "Flow_In_%d" %i: { "$sum": {"$cond": [ { "$eq": [ "$EndStation_Id", i ] }, 1, 0 ] }}
                } }],
                allowDiskUse=True)
            
            result_out = dim.db()[collection].aggregate([
                { "$group": {
                    "_id": { "$subtract": [{ "$subtract": [ "$Start_Date",datetime(1970,1,1) ] }, { "$mod": [{ "$subtract": [ "$Start_Date",datetime(1970,1,1) ] },interval]} ] },
                    "Flow_Out_%d" %i: { "$sum": 
                                       {"$cond": 
                                        [ {"$and" : [ { "$eq": [ "$StartStation_Id", i ] },
                                               { "$ne": [ "$EndStation_Id",0] }] }
                                         , 1, 0 ] }}
                } }],
                allowDiskUse=True)
            
        else:
            result_in = dim.db()[collection].aggregate([
                { "$group": {
                    "_id": { "$subtract": [{ "$subtract": [ "$End_Date",datetime(1970,1,1) ] }, { "$mod": [{ "$subtract": [ "$End_Date",datetime(1970,1,1) ] },interval]} ] },
                    "Flow_In_%d" %i[0]: { "$sum": {"$cond": [ { "$in": [ "$EndStation_Id", i ] }, 1, 0 ] }}
                } }],
                allowDiskUse=True)
            

            result_out = dim.db()[collection].aggregate([
                { "$group": {
                    "_id": { "$subtract": [{ "$subtract": [ "$Start_Date",datetime(1970,1,1) ] }, { "$mod": [{ "$subtract": [ "$Start_Date",datetime(1970,1,1) ] },interval]} ]},
                    "Flow_Out_%d" %i[0]: { "$sum": 
                                       {"$cond": 
                                        [ {"$and" : [ { "$in": [ "$StartStation_Id", i ] },
                                               { "$ne": [ "$EndStation_Id",0] }] }
                                         , 1, 0 ] }}
                } }],
                allowDiskUse=True)
        
        
        result_in = list(result_in)
        FlowIn = pd.DataFrame(result_in)
        
        result_out = list(result_out)
        FlowOut = pd.DataFrame(result_out)


        FlowIn['Date'] = pd.to_datetime(FlowIn['_id'].astype(int), unit='ms')
        FlowOut['Date'] = pd.to_datetime(FlowOut['_id'].astype(int), unit='ms') 
        
        FlowIn.drop(columns=['_id'], inplace=True)
        FlowOut.drop(columns=['_id'], inplace=True)

        Flows = pd.merge(Flows, FlowOut, on = 'Date', how = 'outer')
        Flows = pd.merge(Flows, FlowIn, on = 'Date', how = 'outer')

    Flows.fillna(0, inplace = True)
    
    Flows.to_csv(output)

def flow_nosql_intTimestamp(stationlist, start_date, end_date, interval, output, collection):
    '''Take list of stations (or lists of stations) and produce flow in and out data in time intervals specified'''

    intervalSeconds=interval*60*1000

    TimeStep= pd.date_range(start_date, end_date, freq = str(interval)+"min")

    TimeFrame = pd.DataFrame(index = range(len(TimeStep)), columns = ['Date', 'DoW', 'Month'])
    TimeFrame['Date'] = TimeStep

    TimeFrame['DoW'] = TimeFrame['Date'].apply(lambda x: x.to_datetime().date().weekday())
    TimeFrame['Month'] = TimeFrame['Date'].apply(lambda x: x.to_datetime().date().month)
    Flows = TimeFrame
    
    dim = dimension.Connect('/home/jovyan/projects/config_txt.m')

    for i in stationlist:
        
        if isinstance(i, int):
            if i % 50==0:
                print(str(i))
            
            result_in = dim.db()[collection].aggregate([
                { "$group": {
                    "_id": { "$subtract": ["$End_Date", { "$mod": ["$End_Date",interval]} ] },
                    "Flow_In_%d" %i: { "$sum": {"$cond": [ { "$eq": [ "$EndStation_Id", i ] }, 1, 0 ] }}
                } }],
                allowDiskUse=True)
            
            result_out = dim.db()[collection].aggregate([
                { "$group": {
                    "_id": { "$subtract": ["$Start_Date", { "$mod": ["$Start_Date",interval]} ] },
                    "Flow_Out_%d" %i: { "$sum": 
                                       {"$cond": 
                                        [ {"$and" : [ { "$eq": [ "$StartStation_Id", i ] },
                                               { "$ne": [ "$EndStation_Id",0] }] }
                                         , 1, 0 ] }}
                } }],
                allowDiskUse=True)
            
        else:
            result_in = dim.db()[collection].aggregate([
                { "$group": {
                    "_id": { "$subtract": ["$End_Date", { "$mod": ["$End_Date",interval]} ] },
                    "Flow_In_%d" %i[0]: { "$sum": {"$cond": [ { "$in": [ "$EndStation_Id", i ] }, 1, 0 ] }}
                } }],
                allowDiskUse=True)
            

            result_out = dim.db()[collection].aggregate([
                { "$group": {
                    "_id": { "$subtract": ["$Start_Date", { "$mod": ["$Start_Date",interval]} ] },
                    "Flow_Out_%d" %i[0]: { "$sum": 
                                       {"$cond": 
                                        [ {"$and" : [ { "$in": [ "$StartStation_Id", i ] },
                                               { "$ne": [ "$EndStation_Id",0] }] }
                                         , 1, 0 ] }}
                } }],
                allowDiskUse=True)
        
        result_in = list(result_in)
        FlowIn = pd.DataFrame(result_in)
        
        result_out = list(result_out)
        FlowOut = pd.DataFrame(result_out)


        FlowIn['Date'] = pd.to_datetime(FlowIn['_id'].astype(int), unit='ms')
        FlowOut['Date'] = pd.to_datetime(FlowOut['_id'].astype(int), unit='ms') 
        
        FlowIn.drop(columns=['_id'], inplace=True)
        FlowOut.drop(columns=['_id'], inplace=True)

        Flows = pd.merge(Flows, FlowOut, on = 'Date', how = 'outer')
        Flows = pd.merge(Flows, FlowIn, on = 'Date', how = 'outer')

    Flows.fillna(0, inplace = True)
    
    Flows.to_csv(output)


def group_pairs(l):
    '''Takes a list of lists and merges those lists which have common elements'''
    len_l = len(l)
    i = 0
    while i < (len_l - 1):
        for j in range(i + 1, len_l):

        # i,j iterate over all pairs of l's elements including new elements from merged pairs. We use len_l because len(l) may change as we iterate
            i_set = set(l[i])
            j_set = set(l[j])

            if len(i_set.intersection(j_set)) > 0:
            # Remove these two from list
                l.pop(j)
                l.pop(i)

            # Merge them and append to the orig. list
                ij_union = list(i_set.union(j_set))
                l.append(ij_union)

            # len(l) has changed
                len_l -= 1

            # adjust 'i' because elements shifted
                i -= 1

            # abort inner loop, continue with next l[i]
                break

        i += 1

    return l


def group_stations(stations, Pairs):
    '''Takes a list of stations and list of paired stations and produce conbined list of single stations and clusters'''

    stations=[int(i) for i in stations]

    liststations=[]

    for station in stations:
        liststations.append([station]) 
    
    liststations.extend(Pairs)
    
    groups=group_pairs(liststations)
    
    for i,group in enumerate(groups):
        if len(group)==1:
            groups[i]=group[0]
            
    return groups



def group_flows(file, stations , output):
    '''Take existing flow file and group stations according to list stations'''
    Flow = pd.read_csv(file)
    Flow.drop('Unnamed: 0',axis=1, inplace=True)
    Flow.drop('Month',axis=1, inplace=True)
    Flow['Date'] = pd.to_datetime(Flow['Date'], format='%Y-%m-%d %H:%M:%S')
    
    for group in stations:
        if not isinstance(group,int):
            for j, station in enumerate(group):
                if j != 0:
                    Flow['Flow_In_'+str(group[0])] = Flow['Flow_In_'+str(group[0])]+Flow['Flow_In_'+str(station)]
                    Flow['Flow_Out_'+str(group[0])] = Flow['Flow_Out_'+str(group[0])]+Flow['Flow_Out_'+str(station)]
                    Flow.drop('Flow_In_'+str(station), axis=1, inplace=True)
                    Flow.drop('Flow_Out_'+str(station), axis=1, inplace=True)
    Flow.to_csv(output)


def delete_stations(input, stations, output):
    Flow = pd.read_csv(input)
    Flow.drop('Unnamed: 0',axis=1, inplace=True)
    i = 0
    for station in stations:
        try:
            Flow.drop('Flow_In_'+str(station), axis=1, inplace=True)
            Flow.drop('Flow_Out_'+str(station), axis=1, inplace=True)
            i +=1
        except Exception as e:
            print(str(e))
    print('number of deleted stations: %d' %i)
    Flow.to_csv(output)



def generate_training_data(file, output, hours=False, days=False, stepsperhour=4, random_order=True):
    '''Take flowdata and transform  into correct format for training neural network'''
    s=stepsperhour
    Flows = pd.read_csv(file)
    Flows.drop('Unnamed: 0', axis=1, inplace=True)
    Flows['Date'] = pd.to_datetime(Flows['Date']).apply(lambda x: x.hour).astype('int')
    
    num_stations = int(len(Flows.ix[0, 3: ].as_matrix().astype('float'))/2)
    num_times = len(Flows['Date'].tolist())
    num_inputs = 16 * num_stations + 3
    num_outputs = 2* num_stations
    
    XY = np.zeros((num_times-(168*s + 1), num_inputs + num_outputs), dtype='int')
    X = np.zeros((num_times-(168*s + 1), num_inputs), dtype='int')
    Y = np.zeros((num_times-(168*s + 1), num_outputs), dtype='int')
    for i, row in enumerate(range((168*s + 1), num_times-s)):
        if i % 10000 == 0:
            print(i)
     
     
        Input = Flows.ix[[row-168*s, row-(72*s), row-(48*s), row-(24*s), row-4, row-3, row-2, row-1], 3: ].as_matrix().astype('float').reshape(1, -1)[0]
        Output = Flows.ix[row+s, 3: ].as_matrix().astype('float').reshape(1, -1)[0]
        Input = np.append(Input, [Flows.ix[row+s, 0], Flows.ix[row+s, 1], Flows.ix[row+s, 2]])
        
             
        XY[i, :num_inputs] = Input
        XY[i, num_inputs:] = Output

    if hours:
        mask = (XY[:,-num_outputs-3]<0)
        for hour in hours:
            mask2 = (XY[:,-num_outputs-3]==hour)
            mask = mask | mask2
        print(len(mask))
        print(XY.shape)
        XY=XY[mask,:]
    if days:
        mask = (XY[:,-num_outputs-2]<0)
        for day in days:
            mask2 = (XY[:,-num_outputs-2]==day)
            mask = mask | mask2
        XY=XY[mask,:]
     
    if random_order:
        np.random.shuffle(XY)
    X = XY[:, :num_inputs]
    Y = XY[:, num_inputs:]
    np.savetxt(output+ "Input.csv", X, delimiter = ",", fmt = '%d')
    np.savetxt(output + "Output.csv", Y, delimiter = ",", fmt = '%d')



def average_grouped_flows(file, outputfile, days, format="net", plot=False, engine=None):
    '''Take Flow Data and produce average day of listed days and optionally prepare for plotting'''

    Flow= pd.read_csv(file)
    Flow.drop('Unnamed: 0', axis = 1, inplace = True)
    Flow.drop('Month', axis = 1, inplace = True)
    Flow['Date'] = pd.to_datetime(Flow['Date'], format = '%Y-%m-%d %H:%M:%S')
    Flow['Date'] = Flow['Date'].apply(lambda x : x.time())
    Flow = Flow[Flow.DoW.isin(days)]
    Flow = Flow.groupby('Date').sum()


    if plot:
        stations = pd.read_sql_query("SELECT id FROM Stations", engine)

        for i, row in stations.iterrows():
            try:
                ColumnLocation = Flow.columns.get_loc("Flow_In_" + str(row['id']))
                Flow.insert(ColumnLocation + 1,'Latitude_' + str(row['id']), row['lat'])
                Flow.insert(ColumnLocation + 2,'Longitude_' + str(row['id']), row['long'])
            except:
                pass
    
    if format == "net":
        stations = pd.read_sql_query("SELECT id FROM Stations", engine)
        for i, row in stations.iterrows():
            try:
                ColumnLocation = Flow.columns.get_loc("Flow_In_" + str(row['id']))
                Flow.insert( ColumnLocation + 1, 'Sum_' + str(row['id']), Flow['Flow_In_' + str(row['id'])] + Flow['Flow_Out_' + str(row['id'])])
                Flow.insert( ColumnLocation + 2, 'Difference_' + str(row['id']), Flow['Flow_In_' + str(row['id'])] - Flow['Flow_Out_' + str(row['id'])])
                Flow.drop('Flow_In_' + str(row['id']), axis=1, inplace = True)
                Flow.drop('Flow_Out_' + str(row['id']), axis=1, inplace = True)
            except:
                pass
    Flow.drop('DoW', axis = 1, inplace = True)
    Flow.to_csv(outputfile)






#from py2neo import Graph
#from py2neo import Node, Relationship, Subgraph
if __name__ == "__main__":
    engine=create_engine('mysql+pymysql://root:s2dspassword1@104.196.168.234/BikeData')

    #stations = pd.read_sql_query("SELECT id FROM Stations", engine)['id'].tolist()
    #graph = Graph("http://neo4j:password@localhost:7474/db/data")
    #query = "MATCH(n:Bike_station),(m:Bike_station) WITH distance(point(n),point(m)) AS dist, n.a_Id AS Id1, m.a_Id AS Id2, n.name AS Name1, m.name AS Name2 WHERE dist<200 RETURN Id1, Name1, Id2, Name2, dist"
    #result = graph.run(query).data()
    #records=list(result)
    #df = pd.DataFrame(records, columns=records[0].keys())
    #df=df[(df.dist!=0)&(df.Id1>df.Id2)].sort_values('dist')
    #Pairs=df[df.dist<80].reset_index()[['Id1','Id2']].as_matrix().tolist()
    #groups=group_stations(stations, Pairs)
    #group_flows('../AllFlows2.csv',groups)
    #groups=range(1,20)
    #generate_training_data('AllFlows250816.csv', engine,"RushHour",[7,8,9,10,11,12,13,14,15,16,17,18,19,20])
    
    
    #StationJourneys = pd.read_sql_query("SELECT StartStation_Id, count(*) AS Number FROM Journeys GROUP BY StartStation_Id ORDER BY Number", engine)
    #stations=StationJourneys[(StationJourneys.Number<20000) & ( ~(StationJourneys.StartStation_Id.isin([672,659,735,361,154,374,336,252,334,352,305,355,208,759,256,594,587,276,579,101,578,479,557,71,552,449,546,66,528,49,425,90,420,421,409,411,408,410,406,407,404,36,384,155,13,381,377,173,354,229,338,335,328,327,186,290,280,250,3,140])))]['StartStation_Id'].tolist()
    #delete_stations('AllFlows250816.csv',stations,'AllFlows250816DeletedStations.csv')
    #generate_training_data('AllFlows250816DeletedStations.csv', engine,"RushHourDeletedStations",[7,8,9,10,11,12,13,14,15,16,17,18,19,20])

    #generate_training_data('AllFlows250816.csv', engine,"RushHour",[7,8,9,10,11,12,13,14,15,16,17,18,19,20],stepsperhour=4)

    #StationJourneys = pd.read_sql_query("SELECT StartStation_Id, count(*) AS Number FROM Journeys GROUP BY StartStation_Id ORDER BY Number", engine)
    #stations=StationJourneys[(StationJourneys.Number<20000) & ( ~(StationJourneys.StartStation_Id.isin([672,659,735,361,154,374,336,252,334,352,305,355,208,759,256,594,587,276,579,101,578,479,557,71,552,449,546,66,528,49,425,90,420,421,409,411,408,410,406,407,404,36,384,155,13,381,377,173,354,229,338,335,328,327,186,290,280,250,3,140])))]['StartStation_Id'].tolist()

    #delete_stations('AllFlows1hour.csv',stations,'AllFlows1HourDeletedStations.csv')

    generate_training_data('AllFlows1hour.csv', engine,"AllStationsAllTimes1hour", stepsperhour=1)

    generate_training_data('AllFlows1hour.csv', engine,"AllStationsRushHour1hour", [7,8,9,10,11,12,13,14,15,16,17,18,19,20],stepsperhour=1)

    generate_training_data('AllFlows1HourDeletedStations.csv', engine,"DeletedStationsRushHour1hour",[7,8,9,10,11,12,13,14,15,16,17,18,19,20], stepsperhour=1)

    generate_training_data('AllFlows250816DeletedStations.csv', engine,"RushHourDeletedStations",[7,8,9,10,11,12,13,14,15,16,17,18,19,20])